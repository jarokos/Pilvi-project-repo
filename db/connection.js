var Sequelize = require('sequelize');

var sequelize = new Sequelize('foorumi', 'root', 'root', {
  dialect: 'sqlite',
  storage: '/home/ubuntu/workspace/db/database.sqlite'
});

module.exports = {
  DataTypes: Sequelize,
  sequelize: sequelize
};
