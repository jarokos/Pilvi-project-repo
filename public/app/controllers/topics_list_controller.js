FoorumApp.controller('TopicsListController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  Api.getTopics().success(function(topics){
    $scope.topics = topics;    
  });
  
  $scope.addTest = function(){
      Api.addTopic($scope.newTopic).success(function(added){
          $location.path("/topics/"+added.id)
      });
  }
 
});
