FoorumApp.controller('ShowMessageController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän
   $scope.id = $routeParams.id
  Api.getMessage($scope.id).success(function(message){
      $scope.message = message
  });
  
  $scope.sendReply = function(){
    Api.addReply($scope.newReply, $scope.message.id).success(function(result){
       console.log("reply added");
       $location.path("/"+$scope.message.id)
    });
  }
});
