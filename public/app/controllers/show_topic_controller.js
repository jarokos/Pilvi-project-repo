FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän
  $scope.id = $routeParams.id
  Api.getTopic($scope.id).success(function(topic){
      $scope.topic = topic
  });
  
  $scope.sendMessage = function(){

    Api.addMessage($scope.newMessage, $scope.topic.id).success(function(added){
      $location.path("/messages/"+added.id)
  });
  }
});
