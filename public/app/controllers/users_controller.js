FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  $scope.registerUser = function(){
      Api.register($scope.regUser).then(function(registered){
          Api.login($scope.regUser).then(function(logstat){
            $location.path('/#')
          });
      });
  }
  
  $scope.logIn = function(){
      Api.login($scope.loginUser).then(function(logstat){
          console.log(logstat)
          $location.path('/#')
      });
  }
});
