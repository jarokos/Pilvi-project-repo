var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /topics

// GET /topics
router.get('/', function(req, res, next) {
    // Hae kaikki aihealueet tässä (Vinkki: findAll())

    Models.Topic.findAll().then(function(topics){
       res.status(200).json(topics)
    });
   
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
  // Hae aihealue tällä id:llä tässä (Vinkki: findOne)
  var topicId = req.params.id
  Models.Topic.findOne({ where: {id: topicId}, include: {model: Models.Message,
  include: [{model: Models.User},{model: Models.Reply}]} }).then(function(topic){
    res.status(200).json(topic)
  });
});

// POST /topics
router.post('/', authentication, function(req, res, next) {
  // Lisää tämä aihealue
  var topicToAdd = req.body;
  // Palauta vastauksena lisätty aihealue
  Models.Topic.create({name: topicToAdd.name, description: topicToAdd.description}).then(function(created){
    res.status(200).json(created)
  });
});

// POST /topics/:id/message
router.post('/:id/message', function(req, res, next) {
  // Lisää tällä id:llä varustettuun aihealueeseen...
  var topicId = req.params.id;
  // ...tämä viesti (Vinkki: lisää ensin messageToAdd-objektiin kenttä TopicId, jonka arvo on topicId-muuttujan arvo ja käytä sen jälkeen create-funktiota)
  var messageToAdd = req.body;
  messageToAdd.UserId = req.session.userId;

  // Palauta vastauksena lisätty viesti
   Models.Message.create({title: messageToAdd.title, content: messageToAdd.content, TopicId: topicId, UserId: messageToAdd.UserId}).then(function(added){
    res.status(200).json(added)
  });
  
});

module.exports = router;
